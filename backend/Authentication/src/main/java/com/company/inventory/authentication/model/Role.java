package com.company.inventory.authentication.model;

public enum Role {
    User,
    Admin
}
