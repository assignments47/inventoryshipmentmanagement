package com.company.inventory.authentication.controller;

import com.company.inventory.authentication.dto.AuthenticationRequest;
import com.company.inventory.authentication.dto.AuthenticationResponse;
import com.company.inventory.authentication.dto.RegisterRequest;
import com.company.inventory.authentication.service.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
@CrossOrigin
public class AuthenticationController {

    private final AuthenticationService service;

    @PostMapping("/register")
    public ResponseEntity<AuthenticationResponse> register(RegisterRequest req) {
        return  ResponseEntity.ok(service.register(req));
    }

    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponse> login(@RequestBody AuthenticationRequest req) {
        return  ResponseEntity.ok(service.authenticate(req));
    }

    /*
    Authorization handled by Spring Security
    If JWT not provided or not valid, "forbidden is returned
     */
    @GetMapping("/auth")
    public ResponseEntity<Boolean> authorize() {
        return  ResponseEntity.ok(true);
    }
}
