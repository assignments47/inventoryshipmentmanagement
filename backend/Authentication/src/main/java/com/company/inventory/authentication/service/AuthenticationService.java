package com.company.inventory.authentication.service;

import com.company.inventory.authentication.dto.AuthenticationRequest;
import com.company.inventory.authentication.dto.AuthenticationResponse;
import com.company.inventory.authentication.dto.RegisterRequest;
import com.company.inventory.authentication.model.Role;
import com.company.inventory.authentication.model.User;
import com.company.inventory.authentication.repository.UserRepository;
import com.company.inventory.authentication.util.SaltGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    public AuthenticationResponse authenticate(AuthenticationRequest req) {
        var user = userRepository.findByUsername(req.getUsername()).orElseThrow();
        var passwordSalt = user.getSalt();
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(req.getUsername(), req.getPassword()+passwordSalt));
        var jwt = jwtService.generateToken(user);
        return  AuthenticationResponse.builder().jwt(jwt).build();
    }

    public AuthenticationResponse register(RegisterRequest req) {
        var salt = SaltGenerator.generateRandomSalt();
        var user = User.builder()
                .username(req.getUsername())
                .password(passwordEncoder.encode(req.getPassword()+salt))
                .salt(salt)
                .name(req.getName())
                .role(Role.User)
                .build();
        userRepository.save(user);
        var jwt = jwtService.generateToken(user);
        return  AuthenticationResponse.builder().jwt(jwt).build();
    }
}
