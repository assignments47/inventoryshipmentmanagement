package com.company.inventory.appdataprovider.controller;

import com.company.inventory.appdataprovider.service.AuthorizationService;
import com.company.inventory.appdataprovider.service.DataService;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
public class EventController {

    @Autowired
    //private final KafkaConsumer kafkaConsumer;

    private final AuthorizationService authorizationService;

    private final DataService dataService;

    @GetMapping(value = "/api/events", produces = "text/event-stream")
    public ResponseEntity<Flux<String>> getEvents(@RequestHeader("Authorization") String token) {
        boolean authorized = authorizationService.authorizeRequest(token);

        if (!authorized) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        Flux<String> initialData = dataService.fetchInitialData();


        return ResponseEntity.ok()
                .header("Cache-Control", "no-cache")
                .header("Connection", "keep-alive")
                .body(initialData);
    }
}