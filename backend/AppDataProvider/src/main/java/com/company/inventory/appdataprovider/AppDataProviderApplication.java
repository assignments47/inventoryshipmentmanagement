package com.company.inventory.appdataprovider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppDataProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppDataProviderApplication.class, args);
    }

}
