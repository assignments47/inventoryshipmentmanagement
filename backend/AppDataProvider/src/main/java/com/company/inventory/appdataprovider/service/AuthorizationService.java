package com.company.inventory.appdataprovider.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class AuthorizationService {
    public boolean authorizeRequest(String token) {
        RestTemplate restTemplate = new RestTemplate();

        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + token);
        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

        String otherMicroserviceUrl = "http://localhost:10001";
        try {
            String response = restTemplate.exchange(otherMicroserviceUrl, HttpMethod.GET, entity, String.class).getBody();
            return response.equals("EXPECTED_RESPONSE");
        } catch (Exception e) {
            log.error("Error in authentication response");
            return false;
        }
    }
}
