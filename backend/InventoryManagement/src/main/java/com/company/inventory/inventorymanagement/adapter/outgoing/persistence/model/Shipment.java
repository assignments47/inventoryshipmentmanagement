package com.company.inventory.inventorymanagement.adapter.outgoing.persistence.model;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "shipments")
@Data
public class Shipment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime ShipmentTime;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "ShipmentId")
    private List<ShipmentItem> itemList;
}
