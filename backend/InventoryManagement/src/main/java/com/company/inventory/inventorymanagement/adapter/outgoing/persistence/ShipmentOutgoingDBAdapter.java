package com.company.inventory.inventorymanagement.adapter.outgoing.persistence;

import com.company.inventory.inventorymanagement.adapter.outgoing.persistence.model.Item;
import com.company.inventory.inventorymanagement.adapter.outgoing.persistence.model.Shipment;
import com.company.inventory.inventorymanagement.adapter.outgoing.persistence.model.ShipmentItem;
import com.company.inventory.inventorymanagement.adapter.outgoing.persistence.repository.ItemRepository;
import com.company.inventory.inventorymanagement.adapter.outgoing.persistence.repository.ShipmentRepository;
import com.company.inventory.inventorymanagement.domain.model.Inventory;
import com.company.inventory.inventorymanagement.domain.model.InventoryItem;
import com.company.inventory.inventorymanagement.port.outgoing.ShipmentOutgoingPort;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Slf4j
@Component
public class ShipmentOutgoingDBAdapter implements ShipmentOutgoingPort {
    private final ShipmentRepository shipmentRepository;
    private final ItemRepository itemRepository;

    public ShipmentOutgoingDBAdapter(ShipmentRepository shipmentRepository, ItemRepository itemRepository) {
        this.shipmentRepository = shipmentRepository;
        this.itemRepository = itemRepository;
    }

    @Override
    public boolean makeAvailable(Inventory inventory) {
        try {
            shipmentRepository.save(convert(inventory));
            return true;
        } catch (Exception e) {
            log.error("Error sending data to DB", e);
        }
        return false;
    }

    private Shipment convert(Inventory inventory) {
        var shipment = new Shipment();
        shipment.setShipmentTime(LocalDateTime.now());
        List<ShipmentItem> shipmentItems = new ArrayList<>();
        for (InventoryItem inventoryItem : inventory.getItemList()) {
            var shipmentItem = new ShipmentItem();
            Optional<Item> optionalItem = itemRepository.findById((long) inventoryItem.getItem().getId());
            optionalItem.ifPresent(shipmentItem::setItem);
            shipmentItem.setQuantity(inventoryItem.getQuantity());
            shipmentItems.add(shipmentItem);
        }
        shipment.setItemList(shipmentItems);
        return shipment;
    }
}

