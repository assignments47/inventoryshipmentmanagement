package com.company.inventory.inventorymanagement.port.outgoing;

import com.company.inventory.inventorymanagement.domain.model.Inventory;

public interface ShipmentOutgoingPort {
    boolean makeAvailable(Inventory inventory);
}
