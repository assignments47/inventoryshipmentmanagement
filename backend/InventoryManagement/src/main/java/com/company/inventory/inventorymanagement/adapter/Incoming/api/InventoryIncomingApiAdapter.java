package com.company.inventory.inventorymanagement.adapter.Incoming.api;

import com.company.inventory.inventorymanagement.adapter.Incoming.api.model.ShipmentRequest;
import com.company.inventory.inventorymanagement.adapter.outgoing.kafka.ShipmentOutgoingKafkaAdapter;
import com.company.inventory.inventorymanagement.adapter.outgoing.persistence.ShipmentOutgoingDBAdapter;
import com.company.inventory.inventorymanagement.port.incoming.ShipmentIncomingPort;
import com.company.inventory.inventorymanagement.port.incoming.ShipmentResponse;
import com.company.inventory.inventorymanagement.port.outgoing.ShipmentOutgoingPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InventoryIncomingApiAdapter {
    private final ShipmentIncomingPort shipmentIncomingPort;
    private final ShipmentOutgoingKafkaAdapter shipmentOutgoingKafkaAdapter;
    private final ShipmentOutgoingDBAdapter shipmentOutgoingDBAdapter;

    @Autowired
    public InventoryIncomingApiAdapter(
            final ShipmentIncomingPort shipmentIncomingPort,
            final ShipmentOutgoingKafkaAdapter shipmentOutgoingKafkaAdapter,
            ShipmentOutgoingDBAdapter shipmentOutgoingDBAdapter) {
        this.shipmentIncomingPort = shipmentIncomingPort;
        this.shipmentOutgoingKafkaAdapter = shipmentOutgoingKafkaAdapter;
        this.shipmentOutgoingDBAdapter =  shipmentOutgoingDBAdapter;
    }

    @PostMapping(path = "/inventory/shipment", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> receiveShipment(@RequestBody final ShipmentRequest shipmentRequest) {
        var shipment = shipmentRequest.convert();
        var sucessfulKafka = shipmentIncomingPort.receiveShipment( shipment, shipmentOutgoingKafkaAdapter);
        var sucessfulDB = shipmentIncomingPort.receiveShipment( shipment, shipmentOutgoingDBAdapter);
        return sucessfulKafka == ShipmentResponse.SUCCESS && sucessfulDB == ShipmentResponse.SUCCESS
                ? ResponseEntity.ok("Shipment processed")
                : ResponseEntity.badRequest().body("Failed to process shipment: " + shipmentRequest);
    }
}

