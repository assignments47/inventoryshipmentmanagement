package com.company.inventory.inventorymanagement.domain.model;

import lombok.Data;
import lombok.Builder;

@Data
@Builder
public class InventoryItem {
    private int quantity;
    private Item item;
}
