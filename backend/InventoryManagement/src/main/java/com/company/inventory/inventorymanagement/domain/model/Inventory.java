package com.company.inventory.inventorymanagement.domain.model;

import lombok.Data;
import lombok.Builder;

import java.util.List;

@Data
@Builder
public class Inventory {
    private List<InventoryItem> itemList;

}
