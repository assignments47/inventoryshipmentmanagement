package com.company.inventory.inventorymanagement.domain.service;

import com.company.inventory.inventorymanagement.domain.model.Shipment;
import com.company.inventory.inventorymanagement.domain.model.Inventory;
import com.company.inventory.inventorymanagement.port.incoming.*;
import com.company.inventory.inventorymanagement.port.outgoing.ShipmentOutgoingPort;
import com.company.inventory.inventorymanagement.port.incoming.ShipmentIncomingPort;
import com.company.inventory.inventorymanagement.port.incoming.ShipmentResponse;
import org.springframework.stereotype.Service;

@Service
public class InventoryAppService implements ShipmentIncomingPort {
    private final InventoryDomainService inventoryDomainService = new InventoryDomainService();

    @Override
    public ShipmentResponse receiveShipment(Shipment shipment, ShipmentOutgoingPort shipmentOutgoingPort) {

        boolean isValid = Shipment.isValid.test(shipment);
        if (isValid) {
            Inventory inventory = inventoryDomainService.processShipment(shipment);

            if (!shipmentOutgoingPort.makeAvailable(inventory)) {
                inventoryDomainService.unprocessShipment(shipment);
                isValid = false;
            }
        }

        return isValid ? ShipmentResponse.SUCCESS : ShipmentResponse.FAILURE;
    }
}

