package com.company.inventory.inventorymanagement.adapter.outgoing.persistence.model;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "ShipmentItems")
@Data
public class ShipmentItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ItemId")
    private Item item;

    private int quantity;
}