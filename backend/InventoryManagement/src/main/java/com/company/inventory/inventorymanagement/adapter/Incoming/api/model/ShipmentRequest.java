package com.company.inventory.inventorymanagement.adapter.Incoming.api.model;

import com.company.inventory.inventorymanagement.domain.model.Item;
import com.company.inventory.inventorymanagement.domain.model.Shipment;
import com.company.inventory.inventorymanagement.domain.model.ShipmentItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShipmentRequest {
    private List<ShipmentRequestItem> itemList;

    public Shipment convert() {
        return Shipment.builder()
                .itemList(itemList.stream().map(shipmentRequestItem -> ShipmentItem.builder()
                        .quantity(shipmentRequestItem.getQuantity())
                        .item(Item.builder().id(shipmentRequestItem.getId()).build())
                        .build())
                .collect(Collectors.toList()))
                .build();
    }
}
