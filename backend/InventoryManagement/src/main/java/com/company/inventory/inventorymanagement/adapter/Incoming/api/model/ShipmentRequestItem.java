package com.company.inventory.inventorymanagement.adapter.Incoming.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShipmentRequestItem {
    private int quantity;
    private int id;
}

