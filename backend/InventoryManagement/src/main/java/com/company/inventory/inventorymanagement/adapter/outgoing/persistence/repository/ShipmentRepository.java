package com.company.inventory.inventorymanagement.adapter.outgoing.persistence.repository;

import com.company.inventory.inventorymanagement.adapter.outgoing.persistence.model.Shipment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShipmentRepository extends JpaRepository<Shipment, Long> {
}
