package com.company.inventory.inventorymanagement.domain.service;

import com.company.inventory.inventorymanagement.domain.model.InventoryItem;
import com.company.inventory.inventorymanagement.domain.model.Item;
import com.company.inventory.inventorymanagement.domain.model.Shipment;
import com.company.inventory.inventorymanagement.domain.model.Inventory;

import java.util.ArrayList;
import java.util.Optional;

public class InventoryDomainService {
    private static Inventory inventory;

    public InventoryDomainService() {
        inventory = getInventory();
    }

    public static Inventory getInventory() {
        if (inventory == null) {
            inventory = Inventory.builder().itemList(new ArrayList<>()).build();
        }
        return inventory;
    }

    public Inventory processShipment(final Shipment shipment) {
        shipment.getItemList().stream()
                .map(
                        shipmentItem -> InventoryItem.builder()
                                .quantity(shipmentItem.getQuantity())
                                .item(
                                        Item.builder().id(shipmentItem.getItem().getId()).build()
                                )
                                .build()
                ).forEach(inventoryItem -> {
                    Optional<InventoryItem> foundInventoryItem = inventory.getItemList().stream()
                            .filter(item -> item.getItem().getId() == inventoryItem.getItem().getId())
                            .findFirst();
                    if (foundInventoryItem.isPresent()) {
                        foundInventoryItem.get().setQuantity(foundInventoryItem.get().getQuantity() + inventoryItem.getQuantity());
                    } else {
                        inventory.getItemList().add(inventoryItem);
                    }
                });
        return inventory;
    }

    public Inventory unprocessShipment(final Shipment shipment) {
        shipment.getItemList().stream()
                .map(shipmentItem -> InventoryItem.builder()
                        .quantity(shipmentItem.getQuantity())
                        .item(
                                Item.builder()
                                        .id(shipmentItem.getItem().getId())
                                        .build()
                        )
                        .build())
                .forEach(inventoryItem -> {
                    Optional<InventoryItem> foundInventoryItem = inventory.getItemList().stream()
                            .filter(item -> item.getItem().getId()  == inventoryItem.getItem().getId())
                            .findFirst();
                    if (foundInventoryItem.isPresent()) {
                        if (foundInventoryItem.get().getQuantity() > inventoryItem.getQuantity()) {
                            foundInventoryItem.get().setQuantity(foundInventoryItem.get().getQuantity() - inventoryItem.getQuantity());
                        } else {
                            inventory.getItemList().remove(foundInventoryItem.get());
                        }
                    }
                });
        return inventory;
    }

}
