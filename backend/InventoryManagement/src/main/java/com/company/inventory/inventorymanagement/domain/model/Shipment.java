package com.company.inventory.inventorymanagement.domain.model;

import lombok.Data;
import lombok.Builder;

import java.util.List;
import java.util.function.Predicate;


@Data
@Builder
public class Shipment {
    private List<ShipmentItem> itemList;

    public static Predicate<Shipment> isValid = shipment ->
            shipment != null &&
                    shipment.itemList != null &&
                    shipment.itemList.size() > 0 &&
                    shipment.itemList.stream()
                            .filter(ShipmentItem.isValid)
                            .toList()
                            .size() == shipment.itemList.size();
}
