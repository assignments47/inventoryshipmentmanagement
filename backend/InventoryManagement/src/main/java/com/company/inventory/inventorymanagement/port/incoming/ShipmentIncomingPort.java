package com.company.inventory.inventorymanagement.port.incoming;

import com.company.inventory.inventorymanagement.domain.model.Shipment;
import com.company.inventory.inventorymanagement.port.outgoing.ShipmentOutgoingPort;

public interface ShipmentIncomingPort {
    ShipmentResponse receiveShipment(Shipment shipment, ShipmentOutgoingPort shipmentOutgoingPort);
}
