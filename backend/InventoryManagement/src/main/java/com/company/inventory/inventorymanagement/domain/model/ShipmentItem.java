package com.company.inventory.inventorymanagement.domain.model;

import java.util.function.Predicate;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ShipmentItem {
    private int quantity;
    private Item item;

    public static Predicate<ShipmentItem> isValid = shipmentItem ->
            shipmentItem != null &&
                    shipmentItem.quantity > 0 &&
                    Item.isValid.test(shipmentItem.item);
}

