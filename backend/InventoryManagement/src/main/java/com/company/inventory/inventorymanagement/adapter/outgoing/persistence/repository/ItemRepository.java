package com.company.inventory.inventorymanagement.adapter.outgoing.persistence.repository;

import com.company.inventory.inventorymanagement.adapter.outgoing.persistence.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
}
