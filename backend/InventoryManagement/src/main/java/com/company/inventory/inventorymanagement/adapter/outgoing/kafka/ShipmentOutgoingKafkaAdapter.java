package com.company.inventory.inventorymanagement.adapter.outgoing.kafka;

import com.company.inventory.inventorymanagement.domain.model.Inventory;
import com.company.inventory.inventorymanagement.port.outgoing.ShipmentOutgoingPort;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class ShipmentOutgoingKafkaAdapter implements ShipmentOutgoingPort {
    private final KafkaTemplate<String, String> kafkaTemplate;

    public ShipmentOutgoingKafkaAdapter(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @Override
    public boolean makeAvailable(Inventory inventory) {
        try {
            CompletableFuture<SendResult<String, String>> completableFuture = kafkaTemplate.send("inventoryTopic",
                    new ObjectMapper().writeValueAsString(inventory.getItemList()));


            completableFuture.get(TimeUnit.SECONDS.toSeconds(2), TimeUnit.SECONDS);
            return true;
        } catch (Exception e) {
            log.error("Error sending to Kafka", e);
        }
        return false;
    }
}

