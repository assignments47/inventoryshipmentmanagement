package com.company.inventory.inventorymanagement.domain.model;

import lombok.Builder;
import lombok.Data;

import java.util.function.Predicate;

@Builder
@Data
public class Item {
    private int id;

    public static Predicate<Item> isValid = item ->
            item != null && item.id != 0;
}
