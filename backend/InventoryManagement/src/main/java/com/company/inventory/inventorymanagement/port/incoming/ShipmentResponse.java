package com.company.inventory.inventorymanagement.port.incoming;

public enum ShipmentResponse {
    SUCCESS,
    FAILURE
}
